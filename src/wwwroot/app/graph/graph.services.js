﻿(function () {
    'use strict';
    angular.module('com.ximbor.socialgraph.graph')
        .factory('GraphServices', GraphServices);
    GraphServices.$inject = ['$http'];
    function GraphServices($http) {
        return {
            getGraph: function (nodesCount, linksCount) {
                var config = {
                    method: "GET",
                    url: '../api/Graphs?' + 'nodesLimit=' + nodesCount + '&relationsLimit=' + linksCount + '&nodeAttributes=FirstName&nodeAttributes=LastName',
                    timeout: 1 * 60 * 1000
                };

                return $http(config);
            },

            getNodeDetail: function (nodeId) {
                return $http.get('../api/Users/' + nodeId);
            }
        }
    }
})();