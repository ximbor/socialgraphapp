﻿(function () {
    'use strict';

    angular.module('com.ximbor.socialgraph.graph', [
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'oc.lazyLoad'
    ]).config(routes).run();

    routes.$inject = ['$stateProvider'];
    function routes($stateProvider) {
        $stateProvider
            .state('root.home', {
                url: '/',
                views: {
                    '@': {
                        templateUrl: 'app/graph/templates/graph.html',
                        controller: 'GraphCtrl as graphCtrl'
                    }
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'forceGraph',
                            files: [
                                'app/assets/forcegraph3d/3d-force-graph.min.js',
                                'app/assets/threejs/three.min.js',
                                'app/graph/templates/graph.css'
                            ]
                        }]);
                    }]
                },
                data: {
                    title: 'Graph',
                    requiresLoggedinUser: false
                }
            })
        };
})();