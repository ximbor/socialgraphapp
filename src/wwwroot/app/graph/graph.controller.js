﻿(function () {
    'use strict';

    angular.module('com.ximbor.socialgraph.graph')
        .controller('GraphCtrl', GraphCtrl);

    GraphCtrl.$inject = ['$scope', '$state', '$http', '$timeout', 'GraphServices'];
    function GraphCtrl($scope, $state, $http, $timeout, GraphServices) {

        var self = this;

        // Controller properties:
        self.nodesCountList = ["50", "100", "200", "500", "1000", "3000", "5000", "10000"];
        self.linksCountList = ["50", "100", "200", "500", "1000", "2000", "3000"];
        self.renderers = ['images', 'sphere', 'boxes'];
        self.state = { description: "No data loaded.", countdown: 0, running: false };
        self.linksNumber = "100";
        self.nodesNumber = "100";
        self.selectedNode = {};
        self.renderer = null;

        // Controller functions:
        self.loadGraph = loadGraph;
        self.getRenderers = getRenderers;

        /**
         * Init controller.
         */
        function initController() {
            self.renderer = self.renderers[0];
            self.loadGraph();
        }

        /**
         * Get graph rendererers.
         */
        function getRenderers() {
            return [

                /* Add here new renderers. */

                 {
                    name: 'sphere',
                    description: 'Sphere',
                    rscope: function () { return {}; },
                    fn: function (nodeScope, node, scope) { return nodeScope; }
                }
                ,
                 {
                     name: 'boxes',
                     description: 'Boxes',
                     rscope: function () { return {}; },
                     fn: function (nodeScope, node, scope) {
                        var obj = new THREE.Mesh(new THREE.CubeGeometry(10, 10, 10));
                        return obj;
                     }
                 },
                 {
                    name: 'images',
                    description: 'Images',
                    rscope: function () {
                        let images, imgMaterials = [];
                        for (let i = 1; i < 5; i++) {
                            let img = "app/assets/images/user" + i + ".png";
                            let texture = THREE.ImageUtils.loadTexture(img);
                            var material = new THREE.MeshPhongMaterial({ specular: 0x000000, map: texture, transparent: true });
                            imgMaterials.push(material);
                        }
                        let emptyMaterial = new THREE.MeshPhongMaterial({ specular: 0x000000, color: 0x333333 });
                        let transparentMaterial = new THREE.MeshPhongMaterial({ specular: 0x000000 });
                        let plane = new THREE.PlaneGeometry(10, 10, 32);
                        let planeSmall = new THREE.PlaneGeometry(3, 3, 8);
                        let getRandomInt = function (min, max) {
                            return Math.floor(Math.random() * (max - min + 1)) + min;
                        };
                        let getRandomMaterial = function () { return imgMaterials[getRandomInt(0, imgMaterials.length - 1)] };

                        return {
                            imgMaterials: imgMaterials,
                            getRandomInt: getRandomInt,
                            planeGeometry: plane,
                            planeGeometrySmall: planeSmall,
                            emptyMaterial: emptyMaterial,
                            transparentMaterial: transparentMaterial,
                            nodesCount: 0,
                            linksCount: 0,
                            getRandomMaterial: getRandomMaterial
                        }
                    },
                    fn: function (nodeScope, node, rscope) {

                        if (rscope.nodesCount <= 3000) {
                            let materials = [rscope.emptyMaterial, rscope.emptyMaterial, rscope.emptyMaterial,
                            rscope.emptyMaterial, rscope.getRandomMaterial(), rscope.getRandomMaterial()];
                            var obj = new THREE.Mesh(new THREE.BoxGeometry(10, 10, 3, 10, 1, 1), materials);
                            return obj;
                        }
                        else if (rscope.nodesCount <= 8000) {
                            var obj = new THREE.Mesh(rscope.planeGeometry, rscope.getRandomMaterial());
                            obj.material.side = THREE.DoubleSide;
                            return obj;
                        }
                        else {
                            var obj = new THREE.Mesh(rscope.planeGeometrySmall, rscope.getRandomMaterial());
                            obj.material.side = THREE.DoubleSide;
                            return obj;
                        }
                    }
                }
            ];
        }

        /**
         * Load graph
         */
        function loadGraph() {

            function getGraphDataSet() {

                /**
                 * Get node detail string.
                 * @param {any} node
                 */
                const getNodeDetail = function (node) {

                    let firstName = "", lastName = "";
                    node.attributes.forEach(attr => {
                        if (attr.name === "FirstName") { firstName = attr.value; }
                        if (attr.name === "LastName") { lastName = attr.value; }
                    });
                    return '<div style="color:#333;background:white;width:auto;height:auto;padding:3px;font-size:12px;opacity:0.8;">' + firstName + " " + lastName + '</div>';
                }

                /**
                 * Get graph cold down time
                 * @param {any} nodesCount
                 * @param {any} linksCount
                 */
                function getCooldownTime(nodesCount, linksCount) {
                    if (nodesCount <= 50) { return 1000; }
                    if (nodesCount <= 200) { return 3000; }
                    if (nodesCount <= 500) { return 5000; }
                    if (nodesCount <= 1000) { return 8000; }
                    if (nodesCount <= 3000) { return 12000; }
                    if (nodesCount <= 5000) { return 16000; }
                    if (nodesCount <= 10000) { return 20000; }
                    else {
                        return 25000;
                    }
                }

                /**
                 * Renders each graph node.
                 * @param {any} nodeScope
                 * @param {any} node
                 * @param {any} renderer
                 * @param {any} rendererScope
                 */
                function renderNode(nodeScope, node, renderer, rendererScope) {
                    rendererScope.nodesCount = self.nodesNumber;
                    rendererScope.linksCount = self.linksNumber;
                    return renderer.fn(nodeScope, node, rendererScope);
                }

                /**
                 * Load graph data.
                 * @param {any} Graph
                 */
                const loadGraphData = function (Graph) {

                    self.state.running = true;

                    let nodesCount = self.nodesNumber;
                    let linksCount = self.linksNumber;
                    let cooldownTime = getCooldownTime(nodesCount, linksCount);
                    let selRenderer = getRenderers().filter(x => x.name == self.renderer)[0];
                    let rendererScope = selRenderer.rscope();
                    self.state.description = "Downloading data...";

                    GraphServices.getGraph(nodesCount, linksCount)
                        .then((response) => {
                            let data = response.data;
                            self.state.description = "Loading graph (" + "nodes:" + nodesCount + ", " + "links:" + linksCount + ")...";
                            data.nodes.forEach(node => {
                                node.name = getNodeDetail(node);
                            });

                            Graph
                                .cooldownTicks(300)
                                .cooldownTime(cooldownTime)
                                .nodeAutoColorBy(node => {
                                    return node.attributes.filter(a => a.name == "FirstName")[0].value;
                                })
                                .onNodeClick(node => {

                                    GraphServices.getNodeDetail(node.id)
                                        .then((response) => {
                                            self.selectedNode = response.data;
                                            $('.modal-dialog').draggable({
                                                handle: ".modal-header"
                                            });

                                            self.viewingDetails = true;
                                            $('.modal').modal({
                                                keyboard: false,
                                                show: true
                                            });
                                    });
       
                                })
                                .nodeThreeObject(node => { return renderNode(this, node, selRenderer, rendererScope);})
                                .forceEngine('ngraph')
                                .graphData(data);

                            $timeout(function () {
                                self.state.running = false;
                                self.state.description = "Graph loaded (" + "nodes:" + nodesCount + ", " + "links:" + linksCount + ")";
                                self.state.running = false;
                            }, cooldownTime);

                        });
                };

                return [loadGraphData];
            }

            const getNodeDetail = function (node) {

                let firstName = "", lastName = "";
                node.attributes.forEach(attr => {
                    if (attr.name == "FirstName") { firstName = attr.value; }
                    if (attr.name == "LastName") { lastName = attr.value; }
                });
                return '<div style="color:#333;background:white;width:auto;height:auto;padding:3px;font-size:12px;opacity:0.8;">' + firstName + " " + lastName + '</div>';
            }

            const Graph = ForceGraph3D()(document.getElementById("3d-graph"));

            let curDataSetIdx;
            const dataSets = getGraphDataSet();

            let toggleData;
            (toggleData = function () {
                curDataSetIdx = curDataSetIdx === undefined ? 0 : (curDataSetIdx + 1) % dataSets.length;
                const dataSet = dataSets[curDataSetIdx];

                Graph.resetProps(); // Wipe current state
                dataSet(Graph);     // Load data set

                document.getElementById('graph-data-description').innerHTML = dataSet.description ? 'Viewing ' + dataSet.description : '';
            })(); // IIFE init
        }

        // Load graph and renderers on start up:
        initController();
    }


})();