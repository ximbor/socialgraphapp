﻿
(function () {
    'use strict';
    angular.module('com.ximbor.socialgraph', [
        'ui.router',
        'toastr',
        'com.ximbor.socialgraph.graph'
    ])
    .config(routes)
    .config(configureHttp)
    .run(run);
    routes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function routes($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when('', '/');
        $stateProvider
        .state('root', {
            abstract: true
        });
    }
    run.$inject = ['$rootScope', '$document', '$state', '$stateParams'];
    function run($rootScope, $document, $state, $stateParams, UsersService) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
            if (!toState.data || !toState.data.requiresLoggedinUser) {
                return;
            }
        });
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            console.error('State change error');
            console.log(error);
            $state.go('root.home');
        });
        $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
            console.error('State change not found');
            console.log(unfoundState.options);
            $state.go('root.home');
        });
        $document[0].addEventListener("visibilitychange", function () {
            $rootScope.$broadcast('$visibilitychange', $document[0].hidden);
        });

        $rootScope.app = {};
    }
	configureHttp.$inject = ['$httpProvider'];
    function configureHttp($httpProvider) {
		if (!$httpProvider.defaults.headers.get) {
			$httpProvider.defaults.headers.get = {};    
		}
    }
})();