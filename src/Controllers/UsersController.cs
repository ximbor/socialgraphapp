﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocialGraphApp.Models;

namespace SocialGraphApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : BaseSocialGraphController
    {
        public UsersController(SocialGraphContext context):base(context)
        {
        }

        [HttpGet]
        public IEnumerable<User> GetUsers()
        {
            return _context.Users;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser([FromRoute] int id)
        {
            return await GetSingleEntity<User>(id, _context.Users);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] int id, [FromBody] User user)
        {
            return await PutSingleEntity<User>(id, user, _context.Users);
        }

        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] User user)
        {
            return await PostEntity<User>("GetUser", user, _context.Users);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] int id)
        {
            return await DeleteEntity<User>(id, _context.Users);
        }

    }
}