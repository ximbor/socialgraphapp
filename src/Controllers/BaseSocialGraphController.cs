﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SocialGraphApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace SocialGraphApp.Controllers
{
    public class BaseSocialGraphController : Controller
    {

        public BaseSocialGraphController(SocialGraphContext context)
        {
            try
            {
                _context = context;
            }
            catch (Exception) {
                _context = context;
            }
            
        }

        public readonly SocialGraphContext _context;

        public DbSet<T> GetEntities<T>() where T : SocialGraphEntity
        {
            return _context.Set<T>();
        }

        public async Task<IActionResult> GetSingleEntity<T>(int id, DbSet<T> entities) where T : SocialGraphEntity
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await entities.SingleOrDefaultAsync(m => m.Key == id);

            if (entity == null)
            {
                return NotFound();
            }

            entity.EagerLoad(_context);

            return Ok(entity);
        }

        public async Task<IActionResult> PutSingleEntity<T>(int id, T entity, DbSet<T> entities) where T : SocialGraphEntity
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entity.Key)
            {
                return BadRequest();
            }

            _context.Entry(entity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntityExists<T>(id, entities))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        protected bool EntityExists<T>(int id, DbSet<T> entities) where T : SocialGraphEntity
        {
            return entities.Any(e => e.Key == id);
        }

        public async Task<IActionResult> PostEntity<T>(string actionName, T entity, DbSet<T> entities) where T : SocialGraphEntity
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            entities.Add(entity);
            await _context.SaveChangesAsync();

            return CreatedAtAction(actionName, new { id = entity.Key }, entity);
        }

        public async Task<IActionResult> DeleteEntity<T>(int id, DbSet<T> entities) where T : SocialGraphEntity
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await entities.SingleOrDefaultAsync(m => m.Key == id);
            if (entity == null)
            {
                return NotFound();
            }

            entities.Remove(entity);
            await _context.SaveChangesAsync();

            return Ok(entity);
        }


        public async Task<IActionResult> GetEntitiesByPredicate<T>(Predicate<T> predicate, DbSet<T> entities) where T : SocialGraphEntity
        {
             var result = await entities.Where(m => predicate.Invoke(m)).ToListAsync<T>();

            if (result == null)
            {
                return NotFound();
            }

            result.ForEach( x => x.EagerLoad(_context));

            return Ok(result);
        }


    }
}