﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocialGraphApp.Models;

namespace SocialGraphApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Graphs")]
    public class GraphsController : BaseSocialGraphController
    {
        public GraphsController(SocialGraphContext context):base(context)
        {
        }

        [HttpGet]
        public async Task<Graph>BuildGraph([FromQuery]List<string> nodeAttributes,[FromQuery] int nodesLimit,[FromQuery] int relationsLimit)
        {
            var rand = new Random();
            var nodes = await _context.Users.Select(user => new GraphNode(user, nodeAttributes))
                        .Take(nodesLimit).ToListAsync();
            var relationships = Enumerable.Range(0, relationsLimit - 1).Select(index =>
                new Relationship()
                {
                    Id = index,
                    Source = rand.Next(nodesLimit),
                    Target = rand.Next(nodesLimit)
                }
            ).ToList();

            return new Graph()
            {
                Links = relationships,
                Nodes = nodes
            };

        }

    }
}