﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocialGraphApp.Models;

namespace SocialGraphApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Relationships")]
    public class RelationshipsController : BaseSocialGraphController
    {
        public RelationshipsController(SocialGraphContext context):base(context)
        {
        }

        [HttpGet]
        public IEnumerable<Relationship> GetRelationships()
        {
            return _context.Relationships
                .Include(r => r.SourceUser)
                .Include(r => r.TargetUser);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRelationship([FromRoute] int id)
        {
            return await GetSingleEntity<Relationship>(id, _context.Relationships);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutRelationship([FromRoute] int id, [FromBody] Relationship user)
        {
            return await PutSingleEntity<Relationship>(id, user, _context.Relationships);
        }

        [HttpPost]
        public async Task<IActionResult> PostRelationship([FromBody] Relationship user)
        {
            return await PostEntity<Relationship>("GetRelationship", user, _context.Relationships);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRelationship([FromRoute] int id)
        {
            return await DeleteEntity<Relationship>(id, _context.Relationships);
        }

    }
}