﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialGraphApp.Models
{
    public class NodeAttribute
    {

        public string Name { get; set; }

        public dynamic Value { get; set; }

    }
}
