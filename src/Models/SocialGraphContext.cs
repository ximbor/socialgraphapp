﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System.Collections.Generic;
using System.Linq;

namespace SocialGraphApp.Models
{
    public class SocialGraphContext : DbContext
    {
        public SocialGraphContext(DbContextOptions<SocialGraphContext> options) : base(options)
        {
        }

        public SocialGraphContext(): base()
        {
            
        }

        private const string NODES_DATA_SOURCE          = "./App_data/nodes_10k.csv";
        private const string RELATIONSHIPS_DATA_SOURCE  = "./App_data/relationships_5k.csv";

        public DbSet<User> Users { get; set; }
        public DbSet<Relationship> Relationships { get; set; }

        public string GetNodesSourceName() {
            return NODES_DATA_SOURCE;
        }

        public string GetRelationshipsSourceName()
        {
            return RELATIONSHIPS_DATA_SOURCE;
        }

    }

}
