﻿using SocialGraphApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System.Collections.Generic;
using System.Linq;

public static class SocialGraphDbInitializer
{

    public static void Initialize(SocialGraphContext context)
    {

        context.Database.EnsureCreated();

        // Look for any Currency
        if (context.Users.Any())
        {
            return;   // DB has been seeded
        }

        Build(context);

    }

    public static void Build(SocialGraphContext context)
    {
        using (context)
        {
            BuildUsers(context); context.SaveChanges();
            BuildRelationships(context); context.SaveChanges();
        }
    }

    private static void BuildUsers(SocialGraphContext context)
    {

        Dictionary<string, int> headers = new Dictionary<string, int>();

        context.Users.AddRangeAsync(
            System.IO.File.ReadAllLines(context.GetNodesSourceName())
                .Select((line,index) => {

                    string[] items = line.Split(',');

                    if (index > 0)
                    {
                        //Example: seq,first,last,email,gender,state,city,street,province,zip
                        return new User()
                        {
                            Id          = int.Parse(items[headers["seq"]]),
                            Address     = items[headers["street"]],
                            FirstName   = items[headers["first"]],
                            LastName    = items[headers["last"]],
                            City        = items[headers["city"]],
                            ZipCode     = items[headers["zip"]],
                            Email       = items[headers["email"]],
                            Gender      = items[headers["gender"]]
                        };
                    }
                    else {
                        items.ToList().ForEach((item) => {
                            headers.Add(item, items.ToList().IndexOf(item));
                        });
                        return null;
                    }
            }).Where(item => item != null)
       );

    }

    private static void BuildRelationships(SocialGraphContext context)
    {

        Dictionary<string, int> headers = new Dictionary<string, int>();

        context.Relationships.AddRangeAsync(
            System.IO.File.ReadAllLines(context.GetRelationshipsSourceName())
                .Select((line, index) => {

                    string[] items = line.Split(',');

                    if (index > 0)
                    {
                        //Example: seq,first,last,email,gender,state,city,street,province,zip
                        return new Relationship()
                        {
                            Id = int.Parse(items[headers["seq"]]),
                            Source = int.Parse(items[headers["source"]]),
                            Target = int.Parse(items[headers["target"]])
                        };
                    }
                    else
                    {
                        items.ToList().ForEach((item) => {
                            headers.Add(item, items.ToList().IndexOf(item));
                        });
                        return null;
                    }
                }).Where(item => item != null)
       );

    }
}