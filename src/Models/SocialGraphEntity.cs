﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialGraphApp.Models
{
    public abstract class SocialGraphEntity
    {
        /// <summary>
        /// SocialGraph entity Key.
        /// </summary>
        public abstract int Key { get; }

        public abstract void EagerLoad(SocialGraphContext context);

        public dynamic GetAttribute<T>(string attributeName) where T: SocialGraphEntity
        {
            dynamic value = null;

            Type entityType = ((T)this).GetType();

            (new List<System.Reflection.PropertyInfo>(entityType.GetProperties())).ForEach(prop => {
                if (prop.Name.Equals(attributeName))
                {
                    value = prop.GetValue(this);
                }
            });

            return value;
        }
    }
}
