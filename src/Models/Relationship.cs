﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SocialGraphApp.Models
{
    public class Relationship : SocialGraphEntity
    {

        public Relationship()
        {
        }

        public override void EagerLoad(SocialGraphContext context)
        {
            context.Entry(this).Reference(entity => entity.SourceUser).LoadAsync();
            context.Entry(this).Reference(entity => entity.TargetUser).LoadAsync();
        }

        /// <summary>
        /// Relationship Key.
        /// </summary>
        public override int Key { get { return this.Id; } }

        /// <summary>
        /// Relationship Id.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Relationship's source user.
        /// </summary>
        public int Source { get; set; }
        [ForeignKey("Source")]
        public virtual User SourceUser { get; set; }

        /// <summary>
        /// Relationship's target user.
        /// </summary>
        public int Target { get; set; }
        [ForeignKey("Target")]
        public virtual User TargetUser { get; set; }

    }
}
