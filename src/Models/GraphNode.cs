﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialGraphApp.Models
{
    public class GraphNode
    {
        public int Id { get; set; }

        public IEnumerable<NodeAttribute> Attributes { get; set; }

        public GraphNode() {

        }

        public GraphNode(User user, List<string> Attributes) : base() {

            this.Attributes = Attributes
                                .Select(attributeName => 
                                    new NodeAttribute() {
                                        Name = attributeName,
                                        Value = user.GetAttribute<User>(attributeName)
                                    });
            this.Id = user.Id;
        }
    }
}
