﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SocialGraphApp.Models
{
    public class User : SocialGraphEntity
    {

        public User()
        {
        }

        public override void EagerLoad(SocialGraphContext context)
        {
        }

        /// <summary>
        /// User Key.
        /// </summary>
        public override int Key { get { return this.Id; } }

        /// <summary>
        /// User Id.
        /// </summary>
        [Key]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string Gender { get; set; }

        public string Email { get; set; }

    }
}
