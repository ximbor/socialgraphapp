﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialGraphApp.Models
{
    public class Graph
    {
        /// <summary>
        /// Graph nodes.
        /// </summary>
        public IEnumerable<GraphNode> Nodes { get; set; }

        /// <summary>
        /// Graph edges.
        /// </summary>
        public IEnumerable<Relationship> Links { get; set; }

    }
}
