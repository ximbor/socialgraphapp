# SocialGraphApp

### Prerequisites:
- .NET Crore 2.1.4 SDK (to publish the application)
- Google Chrome or Firefox (not tested on IE or Egde) (to use the application)
- Windows or Linux

### Build the application:
To run the application under Windows:
- open Visual Studio 2017 and compile the application
- "or" launch the script **dist/build.bat**.

To run the application under Linux (not tested):
- open Visual Studio 2017 and compile the application
- "or" run dotnet build from **src** folder.

### Run the application:
To run the application under Windows:
- launch "**dist/runWithChrome.bat**", if you want to run the application in Chrome;
- launch "**dist/runWithFirefox.bat**", if you want to run the application in Firefox;
- launch "**dist/run.bat**", it you want to start the web server, you will be required to browse to the URL **http://localhost:9500**.

To run the application under Linux (not tested):
- run dotnet run from **src** folder.

### REST APIs
This application is based on REST Web APIs, in order to view or edit the entities, Swagger is available at **http://localhost:9500/swagger**.

### Notes
For simplicity this application uses an in memory database, so, restarting the application will clear any changes applied to data.